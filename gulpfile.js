var gulp = require('gulp');
var sass = require('gulp-sass');
var prefix = require('gulp-autoprefixer');

gulp.task('default', function() {
	return gulp.src('scss/**/*.scss')
		.pipe(sass({includePaths:'scss'}))
		.pipe(prefix({browsers:['last 1 version','> 1%','ie 8']}))
		.pipe(gulp.dest('css'));
});
